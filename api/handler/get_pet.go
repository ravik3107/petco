package handler

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
)

func (s *Server) GetPetByID(w http.ResponseWriter, r *http.Request) {
	// read id from vars
	vars := mux.Vars(r)

	res, err := http.Get(fmt.Sprintf("http://petstore-demo-endpoint.execute-api.com/petstore/pets/%v", vars["id"]))
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write([]byte(err.Error()))

		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write([]byte(err.Error()))

		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(body)
}
