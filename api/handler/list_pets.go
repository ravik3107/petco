package handler

import (
	"io/ioutil"
	"net/http"
)

func (s *Server) ListPets(w http.ResponseWriter, r *http.Request) {
	res, err := http.Get("http://petstore-demo-endpoint.execute-api.com/petstore/pets")
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write([]byte(err.Error()))

		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		w.Write([]byte(err.Error()))

		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(body)
}
