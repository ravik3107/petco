package models

type Pet struct {
	ID    int     `json:"id"`
	Type  string  `json:"type"`
	Price float64 `json:"price"`
}
